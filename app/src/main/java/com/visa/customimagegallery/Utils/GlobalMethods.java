package com.visa.customimagegallery.Utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.provider.MediaStore;

import com.visa.customimagegallery.Models.ImageFiles;
import com.visa.customimagegallery.Models.ImageFolder;
import com.visa.customimagegallery.Models.ImageFolderManager;
import com.visa.customimagegallery.R;

import java.util.ArrayList;

public class GlobalMethods {

    public static AlertDialog createAlertDialog(Activity activity, String title, String message) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity, R.style.PhotoEditorAlertDialog);
        dialogBuilder.setMessage(message);
        if (title != null) {
            dialogBuilder.setTitle(title);
        }
        dialogBuilder.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        return dialogBuilder.create();
    }

    public static ImageFolderManager listAllPhotosFromDevice(Context context) {

        final String[] columns = {MediaStore.Images.Media.DATA, MediaStore.Images.Media._ID, MediaStore.Images.Media.BUCKET_DISPLAY_NAME, MediaStore.Images.Media.DISPLAY_NAME};
        final String orderBy = MediaStore.Images.Media.BUCKET_DISPLAY_NAME;

        // Get only png and jpeg images
        final String where = MediaStore.Images.Media.MIME_TYPE + "='image/jpeg'" + " OR " + MediaStore.Images.Media.MIME_TYPE + "='image/png'" + " OR " + MediaStore.Images.Media.MIME_TYPE + "='image/jpg'";

        ImageFolderManager folderManager = new ImageFolderManager();

        // Get photos from external storage
        folderManager = getPhotosFromExternalStorage(context, columns, orderBy, where, folderManager);

        // Get photos from internal storage
        folderManager = getPhotosFromInternalStorage(context, columns, orderBy, where, folderManager);

        return folderManager;
    }

    /* Get Images from external storage into cursor */
    private static ImageFolderManager getPhotosFromExternalStorage(Context context, String[] columns, String orderBy, String where, ImageFolderManager folderManager) {
        try {
            //Stores all the images from the gallery in Cursor
            Cursor cursor = context.getContentResolver().query(
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI, columns, where,
                    null, orderBy);

            folderManager = copyCursorDataInPojo(cursor, folderManager);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return folderManager;
    }

    /* Get Images from internal storage into cursor */
    private static ImageFolderManager getPhotosFromInternalStorage(Context context, String[] columns, String orderBy, String where, ImageFolderManager folderManager) {
        try {
            //Stores all the images from the gallery in Cursor
            Cursor cursor = context.getContentResolver().query(
                    MediaStore.Images.Media.INTERNAL_CONTENT_URI, columns, where,
                    null, orderBy);

            folderManager = copyCursorDataInPojo(cursor, folderManager);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return folderManager;
    }

    /* This method will copy data from cursor to Pojo and return same pojo */
    private static ImageFolderManager copyCursorDataInPojo(Cursor cursor, ImageFolderManager folderManager) {
        try {
            if (cursor != null) {
                for (int i = 0; i < cursor.getCount(); i++) {
                    cursor.moveToPosition(i);
                    int filePathIndex = cursor.getColumnIndex(MediaStore.Images.Media.DATA);
                    int fileIdIndex = cursor.getColumnIndex(MediaStore.Images.Media._ID);
                    int folderDisplayNameIndex = cursor.getColumnIndex(MediaStore.Images.Media.BUCKET_DISPLAY_NAME);
                    int fileDisplayNameIndex = cursor.getColumnIndex(MediaStore.Images.Media.DISPLAY_NAME);

                    ImageFolder imageFolder = new ImageFolder();
                    imageFolder.setFolderName(cursor.getString(folderDisplayNameIndex))
                            .addImageFile(new ImageFiles().setFileId(cursor.getString(fileIdIndex))
                                    .setFilePath(cursor.getString(filePathIndex))
                                    .setFileName(cursor.getString(fileDisplayNameIndex)));

                    folderManager.mapFolderAndFile(imageFolder, Constants.FolderLocation.External);
                }
                // The cursor should be freed up after use with close()
                cursor.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return folderManager;
    }


    /*************************************/
    /*************************************/
    /*******Get files from storage********/
    /*************Starts here*************/
    /*************************************/
    /*************************************/

    // List all Files other than media. Call this public method wherever needed.
    public static ArrayList<ImageFiles> listAllFilesFromDevice(Context context) {

        final String[] columns = {MediaStore.Files.FileColumns.DATA, MediaStore.Files.FileColumns._ID, MediaStore.Files.FileColumns.MIME_TYPE, MediaStore.Files.FileColumns.DISPLAY_NAME};
        final String orderBy = MediaStore.Files.FileColumns.MIME_TYPE;

        // Get all files except media files
        final String where = MediaStore.Files.FileColumns.MEDIA_TYPE + "=" + MediaStore.Files.FileColumns.MEDIA_TYPE_NONE;

        ArrayList<ImageFiles> filesList = new ArrayList<>();

        // Get all files from external storage
        filesList = getFilesFromExternalStorage(context, columns, orderBy, where, filesList);

        // Get all files from internal storage
        filesList = getFilesFromInternalStorage(context, columns, orderBy, where, filesList);

        return filesList;
    }

    /* Get Files other than media from external storage into cursor */
    private static ArrayList<ImageFiles> getFilesFromExternalStorage(Context context, String[] columns, String orderBy, String where, ArrayList<ImageFiles> fileList) {
        try {
            //Stores all the images from the gallery in Cursor
            Cursor cursor = context.getContentResolver().query(
                    MediaStore.Files.getContentUri("external"), columns, where,
                    null, orderBy);

            fileList = copyFilesCursorDataInPojo(cursor, fileList);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return fileList;
    }

    /* Get Files other than media from internal storage into cursor */
    private static ArrayList<ImageFiles> getFilesFromInternalStorage(Context context, String[] columns, String orderBy, String where, ArrayList<ImageFiles> fileList) {
        try {
            //Stores all the images from the gallery in Cursor
            Cursor cursor = context.getContentResolver().query(
                    MediaStore.Files.getContentUri("internal"), columns, where,
                    null, orderBy);

            fileList = copyFilesCursorDataInPojo(cursor, fileList);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return fileList;
    }

    /* This method will copy data from cursor to Pojo and return same pojo */
    private static ArrayList<ImageFiles> copyFilesCursorDataInPojo(Cursor cursor, ArrayList<ImageFiles> fileList) {
        try {
            if (fileList == null) {
                fileList = new ArrayList<>();
            }

            if (cursor != null) {
                for (int i = 0; i < cursor.getCount(); i++) {
                    cursor.moveToPosition(i);
                    int filePathIndex = cursor.getColumnIndex(MediaStore.Files.FileColumns.DATA);
                    int fileIdIndex = cursor.getColumnIndex(MediaStore.Files.FileColumns._ID);
                    int mimeType = cursor.getColumnIndex(MediaStore.Images.Media.MIME_TYPE);
                    int fileDisplayNameIndex = cursor.getColumnIndex(MediaStore.Files.FileColumns.DISPLAY_NAME);
                    if (cursor.getString(fileDisplayNameIndex) != null && cursor.getString(filePathIndex) != null && cursor.getString(mimeType) != null) {
                        fileList.add(new ImageFiles().setFileId(cursor.getString(fileIdIndex))
                                .setFilePath(cursor.getString(filePathIndex))
                                .setFileName(cursor.getString(fileDisplayNameIndex)));
                    }
                }
                // The cursor should be freed up after use with close()
                cursor.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return fileList;
    }
    /*************************************/
    /*************************************/
    /*******Get files from storage********/
    /*************ends here***************/
    /*************************************/
    /*************************************/
}