package com.visa.customimagegallery.Utils;

public class Constants {
    public static final int REQUEST_GALLERY_PERMISSION = 1;
    public static final String SELECTED_IMAGE_URLS = "SELECTED_IMAGE_URLS";

    public enum RequestCode {
        Gallery(0),
        Custom_Gallery(2),
        Setting(1);

        int mValue;

        private RequestCode(int value) {
            this.mValue = value;
        }

        public int toInt() {
            return mValue;
        }
    }

    public enum DisplayMode {
        Folder,
        File;
    }

    public enum FolderLocation {
        Internal,
        External;
    }
}
