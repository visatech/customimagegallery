package com.visa.customimagegallery.Utils;

import android.net.Uri;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

public class ImageLoader {

    // This will load image from local storage
    public static void loadImage(ImageView imageView, Uri imageUri) {
    // Pass the Uri to Glide, to load image from storage
        Glide.with(imageView.getContext())
                .load(imageUri)
                .into(imageView);
    }
}
