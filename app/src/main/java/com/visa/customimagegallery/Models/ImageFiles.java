package com.visa.customimagegallery.Models;

public class ImageFiles {
    private String fileId;
    private String filePath;
    private String fileName;

    public ImageFiles() {
        fileId = "";
        filePath = "";
        fileName = "";
    }

    public String getFileId() {
        return fileId;
    }

    public ImageFiles setFileId(String fileId) {
        this.fileId = fileId;
        return this;
    }

    public String getFilePath() {
        return filePath;
    }

    public ImageFiles setFilePath(String filePath) {
        this.filePath = filePath;
        return this;
    }

    public String getFileName() {
        return fileName;
    }

    public ImageFiles setFileName(String fileName) {
        this.fileName = fileName;
        return this;
    }

}
