package com.visa.customimagegallery.Models;

import com.visa.customimagegallery.Utils.Constants;

import java.util.LinkedHashMap;
import java.util.concurrent.ConcurrentHashMap;

public class ImageFolderManager {

    private LinkedHashMap<String, ImageFolder> imageFolderMap;

    public ImageFolderManager() {
        imageFolderMap = new LinkedHashMap<>();
    }

    public void mapFolderAndFile(ImageFolder folder, Constants.FolderLocation folderLocation) {
        if (imageFolderMap.keySet().contains(folder.getFolderName()) && folderLocation == folder.getFolderLocation()) {
            ImageFolder existingFolder = imageFolderMap.get(folder.getFolderName());
            existingFolder.addAllImageFiles(folder.getImageFiles());

            imageFolderMap.put(folder.getFolderName(), existingFolder);
        } else {
            imageFolderMap.put(folder.getFolderName(), folder);
        }
    }

    public LinkedHashMap<String, ImageFolder> getImageFolderMap() {
        return imageFolderMap;
    }
}
