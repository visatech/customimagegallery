package com.visa.customimagegallery.Models;

import com.visa.customimagegallery.Utils.Constants;

import java.util.ArrayList;

public class ImageFolder {
    private String folderName;
    private ArrayList<ImageFiles> arrImageFiles;
    private Constants.FolderLocation folderLocation = Constants.FolderLocation.External;

    public ImageFolder() {
        folderName = "";
        arrImageFiles = new ArrayList<>();
    }

    public String getFolderName() {
        return folderName;
    }

    public ImageFolder setFolderName(String folderName) {
        this.folderName = folderName;
        return this;
    }

    public ArrayList<ImageFiles> getImageFiles() {
        return arrImageFiles;
    }

    public ImageFolder addImageFile(ImageFiles imageFile) {
        this.arrImageFiles.add(imageFile);
        return this;
    }

    public ImageFolder addAllImageFiles(ArrayList<ImageFiles> arrImageFiles) {
        this.arrImageFiles.addAll(arrImageFiles);
        return this;
    }

    public Constants.FolderLocation getFolderLocation() {
        return folderLocation;
    }

    public void setFolderLocation(Constants.FolderLocation folderLocation) {
        this.folderLocation = folderLocation;
    }
}