package com.visa.customimagegallery.Activities;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;

import com.visa.customimagegallery.Adapters.ImageViewAdapter;
import com.visa.customimagegallery.R;
import com.visa.customimagegallery.Utils.Constants;
import com.visa.customimagegallery.Utils.GlobalMethods;

import java.util.ArrayList;

public class ImageActivity extends AppCompatActivity {

    private RecyclerView rvImageList;
    private ImageViewAdapter imageViewAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image);

        initRecyclerView();

        findViewById(R.id.btnSelectImageCustom).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openGallery();
            }
        });
    }

    private void initRecyclerView() {
        rvImageList = findViewById(R.id.rvImageList);
        rvImageList.setHasFixedSize(true);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayout.HORIZONTAL);
        rvImageList.setLayoutManager(layoutManager);
    }

    //Permissions
    private void requestGalleryPermission() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, Constants.REQUEST_GALLERY_PERMISSION);
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, Constants.REQUEST_GALLERY_PERMISSION);
        }
    }

    private void openGallery() {
        switch (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
            case PackageManager.PERMISSION_GRANTED:
                // If permission is granted, open custom gallery
                Intent intent = new Intent(ImageActivity.this, GalleryActivity.class);
                startActivityForResult(intent, Constants.RequestCode.Custom_Gallery.toInt());
                break;
            case PackageManager.PERMISSION_DENIED:
                boolean shouldShowRequestPermissionRationale;

                if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
                    shouldShowRequestPermissionRationale = shouldShowRequestPermissionRationale(Manifest.permission.READ_EXTERNAL_STORAGE);
                } else {
                    shouldShowRequestPermissionRationale = ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE);
                }

                if (!shouldShowRequestPermissionRationale) {
                    showDialogToOpenSettingScreen();
                } else{
                    requestGalleryPermission();
                }
                break;
        }
    }


    @Override
    public void onActivityResult(int resultCode, int requestCode, Intent data) {
        if (resultCode == Constants.RequestCode.Custom_Gallery.toInt()) {
            if (data != null && data.getStringArrayListExtra(Constants.SELECTED_IMAGE_URLS) != null) {

                // We have set of urls now. Process with the below list now.
                ArrayList<String> selectedImageList = data.getStringArrayListExtra(Constants.SELECTED_IMAGE_URLS);

                imageViewAdapter = new ImageViewAdapter(selectedImageList);
                rvImageList.setAdapter(imageViewAdapter);
            }
        }
    }

    private void showDialogToOpenSettingScreen() {
        final AlertDialog alertDialog = GlobalMethods.createAlertDialog(this, "No access to gallery", "Click on settings and grant permission to access gallery.");
        alertDialog.setCancelable(false);
        alertDialog.setCanceledOnTouchOutside(false);

        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Settings", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent settingsIntent = new Intent();
                settingsIntent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", getPackageName(), null);
                settingsIntent.setData(uri);
                startActivityForResult(settingsIntent, Constants.RequestCode.Setting.toInt());
            }
        });
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                alertDialog.dismiss();
                onBackPressed();
            }
        });
        alertDialog.show();

    }
}
