package com.visa.customimagegallery.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;

import com.visa.customimagegallery.Adapters.FilesListAdapter;
import com.visa.customimagegallery.Adapters.FolderListAdapter;
import com.visa.customimagegallery.Callbacks.ImageCallback;
import com.visa.customimagegallery.Models.ImageFiles;
import com.visa.customimagegallery.Models.ImageFolderManager;
import com.visa.customimagegallery.R;
import com.visa.customimagegallery.Utils.Constants;
import com.visa.customimagegallery.Utils.GlobalMethods;

import java.util.ArrayList;

public class GalleryActivity extends AppCompatActivity implements ImageCallback {

    private Constants.DisplayMode displayMode = Constants.DisplayMode.Folder;
    private RecyclerView rvFolderList;
    private Button btnDone;

    private FolderListAdapter folderListAdapter;
    private FilesListAdapter filesListAdapter;

    private ImageFolderManager folderManager;
    private ArrayList<ImageFiles> imagesList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);

        initViews();
        getImages();
        initRecyclerView();
        showFolderView();
    }

    private void initViews() {
        btnDone = findViewById(R.id.btnDone);
    }

    @Override
    public void onBackPressed() {
        if (displayMode == Constants.DisplayMode.File) {
            showFolderView();
        } else {
            super.onBackPressed();
        }
    }

    private void getImages() {
        folderManager = GlobalMethods.listAllPhotosFromDevice(this);
        folderListAdapter = new FolderListAdapter(folderManager, this);
    }

    private void initRecyclerView() {
        rvFolderList = findViewById(R.id.rvFolderList);
        rvFolderList.setHasFixedSize(true);

        GridLayoutManager layoutManager = new GridLayoutManager(this, 2);
        rvFolderList.setLayoutManager(layoutManager);
    }

    private void showFolderView() {
        btnDone.setVisibility(View.GONE);
        displayMode = Constants.DisplayMode.Folder;

        rvFolderList.setAdapter(folderListAdapter);
    }

    private void showFilesView() {
        btnDone.setVisibility(View.VISIBLE);
        if (imagesList == null) {
            imagesList = new ArrayList<>();
        }
        displayMode = Constants.DisplayMode.File;

        filesListAdapter = new FilesListAdapter(imagesList);
        rvFolderList.setAdapter(filesListAdapter);

        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(GalleryActivity.this, ImageActivity.class);
                intent.putStringArrayListExtra(Constants.SELECTED_IMAGE_URLS, filesListAdapter.getSelectedImageUrls());
                setResult(Constants.RequestCode.Custom_Gallery.toInt(), intent);
                finish();
            }
        });
    }

    @Override
    public void onFolderClicked(String folderName) {
        imagesList = folderManager.getImageFolderMap().get(folderName).getImageFiles();
        showFilesView();
    }
}
