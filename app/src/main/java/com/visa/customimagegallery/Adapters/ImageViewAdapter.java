package com.visa.customimagegallery.Adapters;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.visa.customimagegallery.R;
import com.visa.customimagegallery.Utils.ImageLoader;

import java.io.File;
import java.util.ArrayList;

public class ImageViewAdapter extends RecyclerView.Adapter<ImageViewAdapter.ImageViewHolder> {
    private ArrayList<String> fullUriPath;

    public ImageViewAdapter(ArrayList<String> fullUriPath) {
        this.fullUriPath = fullUriPath;
    }

    @Override
    public int getItemCount() {
        return fullUriPath.size();
    }

    @NonNull
    @Override
    public ImageViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_selected_images, null);
        return new ImageViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ImageViewHolder imageViewHolder, int i) {

        File file = new File(fullUriPath.get(i));
        Uri imageUri = Uri.fromFile(file);

        ImageLoader.loadImage(imageViewHolder.ivImageContainer, imageUri);
    }

    class ImageViewHolder extends RecyclerView.ViewHolder {
        ImageView ivImageContainer;

        ImageViewHolder(View view) {
            super(view);

            ivImageContainer = view.findViewById(R.id.ivImageContainer);
        }
    }
}

