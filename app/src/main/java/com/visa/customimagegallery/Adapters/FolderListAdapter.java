package com.visa.customimagegallery.Adapters;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.visa.customimagegallery.Callbacks.ImageCallback;
import com.visa.customimagegallery.Models.ImageFolder;
import com.visa.customimagegallery.Models.ImageFolderManager;
import com.visa.customimagegallery.R;
import com.visa.customimagegallery.Utils.ImageLoader;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.concurrent.ConcurrentHashMap;

public class FolderListAdapter extends RecyclerView.Adapter<FolderListAdapter.ImageViewHolder> {

    LinkedHashMap<String, ImageFolder> imageFolderMap;
    ArrayList<String> keySets;
    ImageCallback callback;

    public FolderListAdapter(ImageFolderManager folderManager, ImageCallback callback) {
        imageFolderMap = folderManager.getImageFolderMap();
        keySets = new ArrayList<>(imageFolderMap.keySet());
        this.callback = callback;
    }

    @Override
    public int getItemCount() {
        return keySets.size();
    }

    @NonNull
    @Override
    public ImageViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_files, null);
        return new ImageViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ImageViewHolder imageViewHolder, int i) {

        final ImageFolder imageFolder = imageFolderMap.get(keySets.get(i));

        if (imageFolder.getImageFiles().size() > 0) {
            // This will show 1st image as a thumbnail
            File file = new File(imageFolder.getImageFiles().get(0).getFilePath());
            Uri imageUri = Uri.fromFile(file);
            ImageLoader.loadImage(imageViewHolder.ivImageContainer, imageUri);
        }
        imageViewHolder.tvFolderName.setText(imageFolder.getFolderName());

        imageViewHolder.ivImageContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.onFolderClicked(imageFolder.getFolderName());
            }
        });

    }

    class ImageViewHolder extends RecyclerView.ViewHolder {
        ImageView ivImageContainer;
        TextView tvFolderName;

        ImageViewHolder(View view) {
            super(view);

            ivImageContainer = view.findViewById(R.id.ivImageContainer);
            tvFolderName = view.findViewById(R.id.tvFolderName);
        }
    }
}

