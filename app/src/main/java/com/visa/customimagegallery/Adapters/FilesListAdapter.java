package com.visa.customimagegallery.Adapters;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.visa.customimagegallery.Models.ImageFiles;
import com.visa.customimagegallery.R;
import com.visa.customimagegallery.Utils.ImageLoader;

import java.io.File;
import java.util.ArrayList;

public class FilesListAdapter extends RecyclerView.Adapter<FilesListAdapter.ImageViewHolder> {
    ArrayList<ImageFiles> imageFiles;
    ArrayList<String> selectedImageUrls;

    public FilesListAdapter(ArrayList<ImageFiles> imageFiles) {
        this.imageFiles = imageFiles;
        selectedImageUrls = new ArrayList<>();
    }

    public ArrayList<String> getSelectedImageUrls() {
        if (selectedImageUrls == null) {
            selectedImageUrls = new ArrayList<>();
        }
        return selectedImageUrls;
    }

    @Override
    public int getItemCount() {
        return imageFiles.size();
    }

    @NonNull
    @Override
    public ImageViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_files, null);
        return new ImageViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ImageViewHolder imageViewHolder, final int i) {

        // Set the file name on textview
        imageViewHolder.tvFileName.setText(imageFiles.get(i).getFileName());

        // Create file object and get Uri from file path and pass this uri to image loader library to show image
        File file = new File(imageFiles.get(i).getFilePath());
        Uri imageUri = Uri.fromFile(file);
        // Load image in imageview
        ImageLoader.loadImage(imageViewHolder.ivImageContainer, imageUri);

        // Checkbox will randomly checked/ unchecked when scrolled. to avoid this,
        // below code will check if the checkbox was checked. if yes then only update the check box
        if (selectedImageUrls.contains(imageFiles.get(i).getFilePath())) {
            imageViewHolder.chkImageSelect.setChecked(true);
        } else {
            imageViewHolder.chkImageSelect.setChecked(false);
        }


        // When user clicks on image, check the check box
        imageViewHolder.ivImageContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageViewHolder.chkImageSelect.performClick();
            }
        });

        // When user clicks on checkbox, store the file path
        imageViewHolder.chkImageSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (imageViewHolder.chkImageSelect.isChecked()) {
                    selectedImageUrls.add(imageFiles.get(i).getFilePath());
                } else {
                    selectedImageUrls.remove(imageFiles.get(i).getFilePath());
                }
            }
        });

    }

    class ImageViewHolder extends RecyclerView.ViewHolder {
        ImageView ivImageContainer;
        TextView tvFileName;
        CheckBox chkImageSelect;

        ImageViewHolder(View view) {
            super(view);
            initView(view);
        }

        private void initView(View view) {
            ivImageContainer = view.findViewById(R.id.ivImageContainer);
            tvFileName = view.findViewById(R.id.tvFolderName);
            chkImageSelect = view.findViewById(R.id.chkImageSelect);
            chkImageSelect.setVisibility(View.VISIBLE);

            tvFileName.setGravity(Gravity.CENTER_HORIZONTAL);
            view.findViewById(R.id.ivFolderIcon).setVisibility(View.GONE);
        }
    }
}

