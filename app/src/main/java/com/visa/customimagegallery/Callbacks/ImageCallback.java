package com.visa.customimagegallery.Callbacks;

public interface ImageCallback {

    void onFolderClicked(String folderName);
}
